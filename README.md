# WinVMJ Project VOLUNTEER
This repository consists a FeatureIDE project with WinVMJ composer for Volunteer case study, developed by Samuel Tupa Febrian. 

## Requirements
Install Eclipse and required plugins to run this project:
- Eclipse Modeling Tools  (2020-12): https://www.eclipse.org/downloads/packages/release/2020-12/r/eclipse-modeling-tools
- Plugin FeatureIDE 3.9:  http://featureide.cs.ovgu.de/update/v3/
- Plugin WinVMJ composer: https://amanah.cs.ui.ac.id/priceside/winvmj-composer/updatesite
- Java 11
- PostgreSQL


## Getting Started
Make sure that all requirements installed in your Eclipse
- Clone this directory
- Import this directory into Eclipse project (do not include the .git folder)
- Open the project, the structure directory is:
```
.
├── src
├── configs
├── external
├── modules
├── src-gen
├── db.properties
├── feature_to_module.json
├── model.uvl
```
- Edit file `db.properties` with your PostgreSQL credentials


## Generate and Run Product


### Generate Product
Generate product is started with creating a new configuration:
1. The configuration is defined in directory `configs`. 
2. Right click on the project -> NEW -> OTHER -> FeatureIDE -> Configuration File
3. Defined the config file's name that represents the product's name
4. Select required features, right click on the config file -> FeatureIDE -> select `Set As Current Configuration`
5. Generated modules are available in directory `src`
6. Compile the generated module: right click on directory `src` -> FeatureIDE -> WinVMJ -> Compile
The generated application is placed in directory `src-gen`

### Run Product
1. Choose Run -> External Tools -> External Tools Configurations. Define the configuration's name.
2. Fill in location of the generated product, e.g., `${workspace_loc:/Volunteers/src-gen/CommunityPack/run.bat}`
3. Fill in the working directory by defining the product's directory, e.g.,`${${workspace_loc:/Volunteers/src-gen/CommunityPack}}`
4. Click Run
5. If succeed, the product is ready and a list of avalaible endpoints is printed on the console, for example:
```
http://localhost:7776/call/community/list
```
