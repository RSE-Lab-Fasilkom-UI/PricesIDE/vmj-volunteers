package volunteers.community.core;

import volunteers.volunteer.core.VolunteerComponent;

import java.util.*;

public interface Community {
    public int getId();
    public void setId(int id);
	
	public String getName();
    public void setName(String name);
	
	List<VolunteerComponent> getMembers();
    void addMember(VolunteerComponent volunteer);
	
	HashMap<String, Object> toHashMap();
}
