package volunteers.community.core;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.FetchType;

import volunteers.volunteer.core.*;

@Entity(name="community_impl")
@Table(name="community_impl")
public class CommunityImpl extends CommunityComponent {
	
	protected String name;
	
	@ManyToMany(fetch = FetchType.EAGER)
    protected List<VolunteerComponent> members;
	
	public CommunityImpl ()
    {
        this.id = (new Random()).nextInt();
		this.name = "";
        this.members = new ArrayList<>();
    }

	public CommunityImpl (String name)
    {
        this((new Random()).nextInt(), name);
    }

    public CommunityImpl (int id, String name)
    {
        this.id = id;
		this.name = name;
        this.members = new ArrayList<VolunteerComponent>();
    }
	
    public String getName() { return name; }
    public void setName(String name){ this.name = name; }
	
	public List<VolunteerComponent> getMembers(){ return members; }
    public void addMember(VolunteerComponent volunteer){ members.add(volunteer); }

}