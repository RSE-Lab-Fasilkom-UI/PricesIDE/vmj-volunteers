package volunteers.community.core;

import volunteers.volunteer.core.Volunteer;

import java.util.*;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="community_comp")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class CommunityComponent implements Community
{
	@Id
    protected int id;
	
    public int getId() {
        return this.id;
    }
	
    public void setId(int id) {
        this.id = id;
    }


    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> volunteerMap = new HashMap<String,Object>();
        volunteerMap.put("id", getId());
		volunteerMap.put("name", getName());
		volunteerMap.put("members", getMembers().stream().map(Volunteer::toHashMap).
        collect(Collectors.toList()));
		
        return volunteerMap;
    }
}
