package volunteers.community.core;

import javax.persistence.OneToOne;
import javax.persistence.MappedSuperclass;

import java.util.*;

import volunteers.volunteer.core.VolunteerComponent;

import javax.persistence.CascadeType;

@MappedSuperclass
public abstract class CommunityDecorator extends CommunityComponent {
	@OneToOne(cascade=CascadeType.ALL)
    protected CommunityComponent record;
	
	public CommunityDecorator ()
    {
        this.id = (new Random()).nextInt();
    }

    public CommunityDecorator(int id, CommunityComponent record) {
        this.id = id;
        this.record = record;
    }
	
	public CommunityDecorator(CommunityComponent record) {
        this(( new Random()).nextInt(), record);
    }
	
	public CommunityComponent getRecord() { return this.record; }
    public void setRecord(CommunityComponent record) { this.record = record; }

    public String getName() { return record.getName(); }
    public void setName(String name){ record.setName(name); }
	
	public List<VolunteerComponent> getMembers(){ return record.getMembers(); }
    public void addMember(VolunteerComponent volunteer){ record.addMember(volunteer); }
}