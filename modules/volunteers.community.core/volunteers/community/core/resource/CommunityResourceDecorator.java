package volunteers.community.core;

import java.util.*;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

public abstract class CommunityResourceDecorator extends CommunityResourceComponent {

    protected CommunityResourceComponent record;

    public CommunityResourceDecorator(CommunityResourceComponent record) {
        this.record = record;
    }

    public List<HashMap<String,Object>> saveCommunity(VMJExchange vmjExchange) {
        return record.saveCommunity(vmjExchange);
    }

    public Community createCommunity(VMJExchange vmjExchange) {
        return record.createCommunity(vmjExchange);
    }
	
	public HashMap<String, Object> addCommunityMember(VMJExchange vmjExchange) {
        return record.addCommunityMember(vmjExchange);
    }

    public HashMap<String, Object> updateCommunity(VMJExchange vmjExchange) {
        return record.updateCommunity(vmjExchange);
    }

    public HashMap<String, Object> getCommunity(VMJExchange vmjExchange) {
        return record.getCommunity(vmjExchange);
    }

    public List<HashMap<String,Object>> getAllCommunity(VMJExchange vmjExchange) {
        return record.getAllCommunity(vmjExchange);
    }

    public List<HashMap<String,Object>> deleteCommunity(VMJExchange vmjExchange) {
        return record.deleteCommunity(vmjExchange);
    }
}
