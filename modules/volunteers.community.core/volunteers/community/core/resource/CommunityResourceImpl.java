package volunteers.community.core;

import java.util.*;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

import volunteers.community.CommunityFactory;
import volunteers.volunteer.core.*;

public class CommunityResourceImpl extends CommunityResourceComponent {
	
    @Route(url="call/community/save")
    public List<HashMap<String,Object>> saveCommunity(VMJExchange vmjExchange) {
		if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
        Community community = createCommunity(vmjExchange);
        communityDao.saveObject(community);
        return getAllCommunity(vmjExchange);
    }

    public Community createCommunity(VMJExchange vmjExchange) {
        return createCommunity(vmjExchange, (new Random()).nextInt());
    }
    
    public Community createCommunity(VMJExchange vmjExchange, int id) {
		String name = (String) vmjExchange.getRequestBodyForm("name");
        Community community = CommunityFactory.createCommunity("volunteers.community.core.CommunityImpl", id, name);
        return community;
    }

    @Route(url="call/community/update")
    public HashMap<String, Object> updateCommunity(VMJExchange vmjExchange) {
		if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
        Community community = createCommunity(vmjExchange, id);
        communityDao.updateObject(community);
        return community.toHashMap();
    }
	
	@Route(url="call/community/add-member")
	public HashMap<String, Object> addCommunityMember(VMJExchange vmjExchange) {
		if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
		String volunteerIdStr = (String) vmjExchange.getRequestBodyForm("volunteerId");
        int volunteerId = Integer.parseInt(volunteerIdStr);
		Community community = communityDao.getObject(id);
		VolunteerComponent volunteer = communityDao.getProxyObject(VolunteerComponent.class, volunteerId);
		community.addMember(volunteer);
		communityDao.updateObject(community);
        return community.toHashMap();
    }

    @Route(url="call/community/detail")
    public HashMap<String, Object> getCommunity(VMJExchange vmjExchange) {
        String idStr = vmjExchange.getGETParam("id");
        int id = Integer.parseInt(idStr);
        Community community = communityDao.getObject(id);
        try {
            return community.toHashMap();
        } catch (NullPointerException e) {
            HashMap<String, Object> blank = new HashMap<>();
            blank.put("error", "Missing Params");
            return blank;
        }
    }

    @Route(url="call/community/list")
    public List<HashMap<String,Object>> getAllCommunity(VMJExchange vmjExchange) {
        List<Community> communityList = communityDao.getAllObject("community_impl");
        return transformCommunityListToHashMap(communityList);
    }

    // TODO: bisa dimasukin ke kelas util
    public List<HashMap<String,Object>> transformCommunityListToHashMap(List<Community> communityList) {
        List<HashMap<String,Object>> resultList = new ArrayList<HashMap<String,Object>>();
        for(int i = 0; i < communityList.size(); i++) {
            resultList.add(communityList.get(i).toHashMap());
        }

        return resultList;
    }

    @Route(url="call/community/delete")
    public List<HashMap<String,Object>> deleteCommunity(VMJExchange vmjExchange) {
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
        communityDao.deleteObject(id);
        return getAllCommunity(vmjExchange);
    }
}
