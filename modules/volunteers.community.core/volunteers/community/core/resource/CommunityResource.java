package volunteers.community.core;

import java.util.*;

import vmj.routing.route.VMJExchange;

public interface CommunityResource {
    List<HashMap<String,Object>> saveCommunity(VMJExchange vmjExchange);
    HashMap<String, Object> updateCommunity(VMJExchange vmjExchange);
	HashMap<String, Object> addCommunityMember(VMJExchange vmjExchange);
    HashMap<String, Object> getCommunity(VMJExchange vmjExchange);
    Community createCommunity(VMJExchange vmjExchange);
    Community createCommunity(VMJExchange vmjExchange, int id);
    List<HashMap<String,Object>> getAllCommunity(VMJExchange vmjExchange);
    List<HashMap<String,Object>> deleteCommunity(VMJExchange vmjExchange);
}
