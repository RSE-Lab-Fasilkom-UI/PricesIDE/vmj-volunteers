package volunteers.community.core;

import java.util.*;

import vmj.hibernate.integrator.DaoUtil;
import vmj.routing.route.VMJExchange;

public abstract class CommunityResourceComponent implements CommunityResource {
    protected DaoUtil<Community> communityDao;

    public CommunityResourceComponent(){
        this.communityDao = new
            DaoUtil<Community>(volunteers.community.core.CommunityComponent.class);
    }
}
