module volunteers.community.core {
    exports volunteers.community;
    exports volunteers.community.core;
	
	requires volunteers.volunteer.core;
    
    requires vmj.routing.route;
    // https://stackoverflow.com/questions/46488346/error32-13-error-cannot-access-referenceable-class-file-for-javax-naming-re/50568217
    requires java.naming;
    requires vmj.hibernate.integrator;
    requires java.logging;
    
    requires prices.auth.vmj;
    
    opens volunteers.community.core to org.hibernate.orm.core, gson;
}
