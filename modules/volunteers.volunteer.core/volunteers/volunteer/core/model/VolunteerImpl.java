package volunteers.volunteer.core;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name="volunteer_impl")
@Table(name="volunteer_impl")
public class VolunteerImpl extends VolunteerComponent {
	
	protected String name;
    protected String email;
    protected String phone;
	
	public VolunteerImpl ()
    {
        this.id = (new Random()).nextInt();
		this.name = "";
        this.email = "";
        this.phone = "";
    }

	public VolunteerImpl (String name, String email, String phone)
    {
        this((new Random()).nextInt(), name, email, phone);
    }

    public VolunteerImpl (int id, String name, String email, String phone)
    {
        this.id = id;
		this.name = name;
        this.email = email;
        this.phone = phone;
    }
	
    public String getName() { return name; }
    public void setName(String name){ this.phone = phone; }
	
	public String getPhone(){ return phone; }
    public void setPhone(String phone){ this.phone = phone; }
	
	public String getEmail(){ return email; }
    public void setEmail(String email){ this.email = email; }

}