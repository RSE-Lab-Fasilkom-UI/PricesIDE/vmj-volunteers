package volunteers.volunteer.core;

import java.util.*;

public interface Volunteer {
    public int getId();
    public void setId(int id);
	
	public String getName();
    public void setName(String name);
	
	String getPhone();
    void setPhone(String phone);
	
	String getEmail();
    void setEmail(String email);
	
	HashMap<String, Object> toHashMap();
}
