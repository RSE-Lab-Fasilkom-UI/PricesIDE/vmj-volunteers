package volunteers.volunteer.core;

import javax.persistence.OneToOne;
import javax.persistence.MappedSuperclass;

import java.util.Random;

import javax.persistence.CascadeType;

@MappedSuperclass
public abstract class VolunteerDecorator extends VolunteerComponent {
	@OneToOne(cascade=CascadeType.ALL)
    protected VolunteerComponent record;
	
	public VolunteerDecorator ()
    {
        this.id = (new Random()).nextInt();
    }

    public VolunteerDecorator(int id, VolunteerComponent record) {
        this.id = id;
        this.record = record;
    }
	
	public VolunteerDecorator(VolunteerComponent record) {
        this(( new Random()).nextInt(), record);
    }
	
	public VolunteerComponent getRecord() { return this.record; }
    public void setRecord(VolunteerComponent record) { this.record = record; }

    public String getName() { return record.getName(); }
    public void setName(String name){ record.setName(name); }
	
	public String getPhone(){ return record.getPhone(); }
    public void setPhone(String phone){ record.setPhone(phone); }
	
	public String getEmail(){ return record.getEmail(); }
    public void setEmail(String email){ record.setEmail(email); }
}