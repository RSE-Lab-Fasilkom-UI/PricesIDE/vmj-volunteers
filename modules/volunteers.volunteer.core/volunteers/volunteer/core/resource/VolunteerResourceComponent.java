package volunteers.volunteer.core;

import java.util.*;

import vmj.hibernate.integrator.DaoUtil;
import vmj.routing.route.VMJExchange;

public abstract class VolunteerResourceComponent implements VolunteerResource {
    protected DaoUtil<Volunteer> volunteerDao;

    public VolunteerResourceComponent(){
        this.volunteerDao = new
            DaoUtil<Volunteer>(volunteers.volunteer.core.VolunteerComponent.class);
    }
}
