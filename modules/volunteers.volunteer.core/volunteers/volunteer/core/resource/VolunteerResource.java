package volunteers.volunteer.core;

import java.util.*;

import vmj.routing.route.VMJExchange;

public interface VolunteerResource {
    List<HashMap<String,Object>> saveVolunteer(VMJExchange vmjExchange);
    HashMap<String, Object> updateVolunteer(VMJExchange vmjExchange);
    HashMap<String, Object> getVolunteer(VMJExchange vmjExchange);
    Volunteer createVolunteer(VMJExchange vmjExchange);
    Volunteer createVolunteer(VMJExchange vmjExchange, int id);
    List<HashMap<String,Object>> transformVolunteerListToHashMap(List<Volunteer> volunteerList);
    List<HashMap<String,Object>> getAllVolunteer(VMJExchange vmjExchange);
    List<HashMap<String,Object>> deleteVolunteer(VMJExchange vmjExchange);
}
