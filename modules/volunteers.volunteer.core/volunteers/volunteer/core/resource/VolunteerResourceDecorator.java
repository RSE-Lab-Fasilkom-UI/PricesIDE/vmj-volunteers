package volunteers.volunteer.core;

import java.util.*;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

public abstract class VolunteerResourceDecorator extends VolunteerResourceComponent {

    protected VolunteerResourceComponent record;

    public VolunteerResourceDecorator(VolunteerResourceComponent record) {
        this.record = record;
    }

    public List<HashMap<String,Object>> saveVolunteer(VMJExchange vmjExchange) {
        return record.saveVolunteer(vmjExchange);
    }

    public Volunteer createVolunteer(VMJExchange vmjExchange) {
        return record.createVolunteer(vmjExchange);
    }

    public HashMap<String, Object> updateVolunteer(VMJExchange vmjExchange) {
        return record.updateVolunteer(vmjExchange);
    }

    public HashMap<String, Object> getVolunteer(VMJExchange vmjExchange) {
        return record.getVolunteer(vmjExchange);
    }

    public List<HashMap<String,Object>> getAllVolunteer(VMJExchange vmjExchange) {
        return record.getAllVolunteer(vmjExchange);
    }

     public List<HashMap<String,Object>> transformVolunteerListToHashMap(List<Volunteer> financialReportList) {
         return record.transformVolunteerListToHashMap(financialReportList);
     }

    public List<HashMap<String,Object>> deleteVolunteer(VMJExchange vmjExchange) {
        return record.deleteVolunteer(vmjExchange);
    }
}
