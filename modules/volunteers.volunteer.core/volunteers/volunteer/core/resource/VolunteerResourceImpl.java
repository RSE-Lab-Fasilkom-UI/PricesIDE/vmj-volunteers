package volunteers.volunteer.core;

import java.util.*;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

import volunteers.volunteer.VolunteerFactory;

public class VolunteerResourceImpl extends VolunteerResourceComponent {

    @Route(url="call/volunteer/save")
    public List<HashMap<String,Object>> saveVolunteer(VMJExchange vmjExchange) {
		if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
        Volunteer volunteer = createVolunteer(vmjExchange);
        volunteerDao.saveObject(volunteer);
        return getAllVolunteer(vmjExchange);
    }

    public Volunteer createVolunteer(VMJExchange vmjExchange) {
        return createVolunteer(vmjExchange, (new Random()).nextInt());
    }
    
    public Volunteer createVolunteer(VMJExchange vmjExchange, int id) {
		String name = (String) vmjExchange.getRequestBodyForm("name");
		String email = (String) vmjExchange.getRequestBodyForm("email");
		String phone = (String) vmjExchange.getRequestBodyForm("phone");
        Volunteer volunteer = VolunteerFactory.createVolunteer("volunteers.volunteer.core.VolunteerImpl", id, name, email, phone);
        return volunteer;
    }
	
    @Route(url="call/volunteer/update")
    public HashMap<String, Object> updateVolunteer(VMJExchange vmjExchange) {
		if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
        Volunteer volunteer = createVolunteer(vmjExchange, id);
        volunteerDao.updateObject(volunteer);
        return volunteer.toHashMap();
    }

    @Route(url="call/volunteer/detail")
    public HashMap<String, Object> getVolunteer(VMJExchange vmjExchange) {
        String idStr = vmjExchange.getGETParam("id");
        int id = Integer.parseInt(idStr);
        Volunteer volunteer = volunteerDao.getObject(id);
        try {
            return volunteer.toHashMap();
        } catch (NullPointerException e) {
            HashMap<String, Object> blank = new HashMap<>();
            blank.put("error", "Missing Params");
            return blank;
        }
    }

    @Route(url="call/volunteer/list")
    public List<HashMap<String,Object>> getAllVolunteer(VMJExchange vmjExchange) {
        List<Volunteer> volunteerList = volunteerDao.getAllObject("volunteer_impl");
        return transformVolunteerListToHashMap(volunteerList);
    }

    // TODO: bisa dimasukin ke kelas util
    public List<HashMap<String,Object>> transformVolunteerListToHashMap(List<Volunteer> volunteerList) {
        List<HashMap<String,Object>> resultList = new ArrayList<HashMap<String,Object>>();
        for(int i = 0; i < volunteerList.size(); i++) {
            resultList.add(volunteerList.get(i).toHashMap());
        }

        return resultList;
    }

    @Route(url="call/volunteer/delete")
    public List<HashMap<String,Object>> deleteVolunteer(VMJExchange vmjExchange) {
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
        volunteerDao.deleteObject(id);
        return getAllVolunteer(vmjExchange);
    }
}
